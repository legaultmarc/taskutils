import glob
import os
import os.path

def find_subdirs(path):
    subdirs = []
    for dirpath, dirnames, filenames in os.walk(path):
        if dirpath:
            subdirs.append(dirpath)

    return subdirs
