
PREFIXES = {
                1e-12: "p",
                1e-9 : "n",
                1e-6 : "u",
                1e-3 : "m",
                1e-2 : "c",
                1e-1 : "d",
                1e3 : "k",
                1e6 : "M",
                1e9 : "G",
                1e12 : "T",
           }

def metric_prefix(number, round_num = False, precision = None, no_space = False, prefixes = PREFIXES):
    """Formats a number into a simpler form using metric prefixes.
    
    :param number: The number you want to format.
    :type number: float

    :param round_num: A flag that will `round()` your number if set to True.
    :type round_num: bool

    :param precision: The number of digits of precision.
    :type precision: int

    :param no_space: A flag that will remove the space between the number and the metric prefix. It might be more readable to set it to True, but if the file is space delimited, it might introduce problems, that's why you have the option.
    :type no_space: bool

    :param prefixes: A dict of float_value => metric prefix that customizes the allowed prefixes. There is a reasonable one by default which maps the following prefixes: p, n, u, m, c, d, k, M, G and T. Those are the most frequently used in bioinformatics.
    :type prefixes: dict

    :returns: A formatted string.
    :rtype: str

    """
    values = sorted(prefixes)
    p = 0
    i = 0

    while (number / values[i]) >= 1:
        p = i
        i += 1
        if i == len(values):
            break
    
    number /= values[p]
    if round_num: number = int(round(number))
    if precision: num_format = "{{:.{}f}}".format(precision)
    else: num_format = "{}"

    if no_space:
        num_format += "{}"
    else:
        num_format += " {}"
    return num_format.format(number, prefixes[values[p]])
