# TaskUtils

## Launching tasks in MOAB

Use this Python module to launch MOAB tasks from a Python script.

### Simple tasks

e.g.

    import TaskUtils.Launcher.moab as moab
    moab.run("ls -l")

### Advanced features

It is possible to pass a `params` parameter to the `run` function in order to directly inject MOAB parameters when launching the task. A list of available options can be found in CLUMEQ's documentation for MOAB ([here](https://www.clumeq.ca/wiki/index.php/Utilisation_de_Moab)).
As an exemple, if one wanted to be contacted by email when a task starts, aborts or terminates, the following string should be passed as the `params` argument: `"-M email@exemple.com -m bea"`.
The output filenames could also be customised by using `params = "-o output.txt -e output_error.txt"`.
Note that the mandatory options should be set using the `SettingsLoader` module.

### Parallel tasks

For easier parallelization, the `run_array` function can be used. 

e.g.

    li = ["ls -l", "sleep 20", "wc -l a_large_file.txt"]
    moab.run_array(li, jobs_per_node = 1, ret = True, vrb = True)

The above code will return the an `(out, err)` array, where `out` is the standard output (most likely, this will be the task id) and the standard error, which should be `None`. The `jobs_per_node` option allows the user to decide how many tasks should be launched on a given node (depending on the computing power that is needed).

The `vrb` setting can also be useful as it will print everything (commands, task ids) to the screen when launching on moab.

### Settings

You can also customize the different options using either a Python dictionary, a file (with `PARAMETER=value`) or a Python file with the corresponding variables. By default, `TaskUtils.Launcher` will use the `settings.py` which you should create from the sample that is available in this repository.

To customize this, you can use the following methods before running a task:

    import TaskUtils.Launcher.SettingsLoader as sl
    sl.load_from_dict(dictionary)
    sl.load_from_file("path_to_file.py")

The easiest way is probably to simply use the `settings.py` file.
If the dictionary approach is used, the keys are the same as the parameters names in `settings.py`.

    d = {
            "PROJECT_NAME": "abc-000-00",
            "JOB_NAME": "MyJob",
            "WALLTIME": "300",
            "NODES": 1,
            "PPN": 8,
        }

To verify the effective settings, use:

    import TaskUtils.Launcher.moab as moab
    moab.print_settings()

## Tools

### Path

You can use this to easily get all subdirectories (without using `walk`).

e.g.

    import TaskUtils.Tools.Path as tupath
    tupath.find_subdirs("my_dir")

This will return a Python list of the subdirectories for `my_dir`.

### Metric prefixes

To easily convert floats to strings with appropriate metric prefixes, you can use the `TaskUtils.Tools.Units` module. The function `metric_prefixes` will allow you to easily convert floats to their human-readable prefixed notation.
For full documentation on this function, refer to it's docstring (`print metric_prefix.__doc__`)
