import os
import TaskUtils.Launcher
from subprocess import Popen, PIPE

def run(s, ret = False, params = None):
    """Runs a command through moab.

       ret (bool) Determines if the stdout and stderr need to
       be returned to the calling script.

       params (string) Moab commands that can be injected
       when running the task. 
       One could set params = "-M me@exemple.com -m e" to
       be notified when the job ends.
    """

    # Build a command that is understandable by moab
    # from settings parameters.
    settings = TaskUtils.Launcher.SETTINGS
    moab_parameters = ["msub"]
    # Walltime, nodes and ppn
    wt = settings["WALLTIME"]
    nodes = settings["NODES"]
    ppn = settings["PPN"]
    moab_parameters.append("-l")
    moab_parameters.append("walltime={0},nodes={1}:ppn={2}".format(wt, nodes, ppn))

    project_name = settings["PROJECT_NAME"]
    job_name = settings.get("JOB_NAME") if settings.get("JOB_NAME") else "MyTask"  
    moab_parameters.append("-A")
    moab_parameters.append(project_name)
    moab_parameters.append("-N")
    moab_parameters.append(job_name)

    # Keep the cwd
    moab_parameters.append("-d")
    moab_parameters.append(os.getcwd())

    if params:
        for i in params.split(" "):
            moab_parameters.append(i)

    echo_subp = Popen(["echo", s], stdout=PIPE)
    command_subp = Popen(moab_parameters, stdin=echo_subp.stdout, stdout=PIPE)
    out, err = command_subp.communicate()

    out = out.strip("\r\n")
    err = err.strip("\r\n") if err else None

    print out

    if err:
        print err

    if ret:
        return (out, err)

def run_array(li, jobs_per_node = 8, ret = False, params = None, vrb = False):
    """Runs an array of commands on multiple nodes.

    Args:
        li (list): A list of strings for the commands that need to be launched.
        jobs_per_nod (int): The number of jobs to launch per node (Default: 8)

    """

    nodes = {}
    out_dic = {}
    i = 0
    j = 0

    # Dispatch command to different nodes
    for command in li:
        if nodes.get(j) is None:
            nodes[j] = []

        nodes[j].append(command)
        i += 1
        if i == jobs_per_node:
            i = 0
            j += 1

    for i in nodes.keys():
        # Join with amperstands (parallel execution of tasks)
        # and run on moab.
        command_for_node_i = " & ".join(nodes[i])
        out, err = run(command_for_node_i, ret=True, params = params)
        out = out.strip("\r\n")
        err = err.strip("\r\n") if err else None

        if vrb:
            print command_for_node_i
            print out
            print err
            print

        out_dic[i] = {"out": out, "err": err}

    if ret:
        return out_dic

def print_settings():
    print TaskUtils.Launcher.SETTINGS
