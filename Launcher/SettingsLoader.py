import re
import os
import os.path

try:
    import TaskUtils.settings
except ImportError as e:
    print "You need to create the settings.py file in this module's root directory."
    raise e

# Default file location is the root of the module.
DEFAULT_SETTINGS_FILE = os.path.abspath(
                                        os.path.join(os.path.dirname(__file__), "../settings.py")
                                       )

SETTINGS = {}

def load_from_file(f = DEFAULT_SETTINGS_FILE):

    # Base for regular expressions matching known parameters.
    base_regexp = "^({0})\s?=\s?\"?([A-Za-z0-9-]+)\"?"

    # What settings to look for
    names = TaskUtils.settings.all

    # Generates all the corresponding regexps
    regexps = [base_regexp.format(name) for name in names]
    
    # Opens and parses the file into the SETTINGS dictionary.
    with open(f) as file:
        for line in file:
            for regex in regexps:
                match = re.match(regex, line)
                if match:
                    SETTINGS.__setitem__(match.group(1), match.group(2))

def load_from_module():
    # Load settings from the settings module
    for setting_element in TaskUtils.settings.all:
        SETTINGS.__setitem__(setting_element, TaskUtils.settings.__dict__[setting_element])

def load_from_dict(d):
    # Load the settings from a user provided dictionary.
    for setting_element in TaskUtils.settings.all:
        dict_value = d.get(setting_element)
        if dict_value:
            SETTINGS.__setitem__(setting_element, dict_value)

def show_loaded_settings():
    print SETTINGS

def get_settings():
    return SETTINGS
