import SettingsLoader

print "Loading settings from 'settings.py'"
SettingsLoader.load_from_module()
SETTINGS = SettingsLoader.get_settings()
